clc
close all
clear all

%%
% eNAC Model Parameter
load eNAC_Model_Param

% eNAC Task structure
load eNAC_Task 

%%
% Define the reward function
temp1 = eNAC_Task.cost.h;
qf_fun  = matlabFunction(temp1,'vars',{eNAC_Task.cost.x});

temp2 = eNAC_Task.cost.l*eNAC_Task.dt;
q_fun   = matlabFunction(temp2,'vars',{eNAC_Task.cost.x,eNAC_Task.cost.u});

% Transform Controller structure from ILQG convention to eNAC
load ILQG_Controller 
Controller = BaseFcnTrans(ILQG_Controller,10);

%% eNAC Algorithm for adaptation
t_cpu = cputime;

AllCost = zeros(eNAC_Task.max_iteration+1,1);
AllController(1:eNAC_Task.max_iteration+1,1) = Controller;
Allgoods = 1;
num_time_steps = (eNAC_Task.goal_time-eNAC_Task.start_time)/eNAC_Task.dt + 1;

bsim_out = [];
bR = zeros(eNAC_Task.num_rollouts,num_time_steps);

for cnt1 = 1:eNAC_Task.max_iteration+1
    
    % Modify the noise covariance according to the current covariance
    % matrix
    temp = repmat(Controller.theta(end,:)',[1 num_time_steps size(eNAC_Task.random,3)]);
    Task = eNAC_Task;
    Task.random = temp .* eNAC_Task.random;
    
    % Perform a test rollout to evaluate the current solution
    Test_sim_out  = Ballbot_Simulator(eNAC_Model_Param,Task,Controller);
    
    % Save the cost of test rollout
    temp = [q_fun(Test_sim_out.x(:,1:end-1),Test_sim_out.u(:,1:end-1))...
                qf_fun(Test_sim_out.x(:,end))];
    AllCost(cnt1) = sum(temp);
    
    % Print some information out
    fprintf('Cost %2d: %6.4f',cnt1-1,AllCost(cnt1))
    temp = Controller.theta(end,:);
    fprintf('\t \t sigma=[%f, %f, %f]\n',temp(1),temp(2),temp(3))
    
    % Block bad solution and go back randomly to the one of the 5-recent
    % acceptable solutions
    if cnt1 > 1
        if AllCost(cnt1-1)*1.1 < AllCost(cnt1) || isnan(AllCost(cnt1))
            temp = randperm(5);  temp = temp(1)-1;
            temp = max(1,length(Allgoods)-temp);
            Controller = AllController(Allgoods(temp));
            fprintf('Iteration %d is blocked back to %d \n',cnt1-1,Allgoods(temp))
        else
            Allgoods = [Allgoods; cnt1];
        end
    end
    
    if cnt1 > Task.max_iteration
        break;
    end
    
    % Create "Task.num_rollouts" rollouts and save the information in
    % "bsim_out" and "bR"
    for cnt2 = length(bsim_out)+1:Task.num_rollouts
        
        % Simulate a rollout
        sim_out  = Ballbot_Simulator(eNAC_Model_Param,Task,Controller);
        bsim_out = [bsim_out; sim_out];
        
        % Calculate the reward for each time step
        temp1 = -q_fun(sim_out.x(:,1:end-1),sim_out.u(:,1:end-1));
        temp2 = -qf_fun(sim_out.x(:,end));
        bR(cnt2,:) = [temp1 repmat(temp2,1,num_time_steps-length(temp1))];
        
    end
    
    % eNAC_Algorithm (use your eNAC code here)
    [g,V0] = eNAC_Algorithm(bsim_out,bR);
    
    % Update parameters
    Controller.theta = Controller.theta + (5000/cnt1^0.5)*g;
    % Make sure sigmas are always bigger than 0
    Controller.theta(end,:) = max(Controller.theta(end,:),zeros(1,3));
    % Save Controller
    AllController(cnt1+1) = Controller;
    
    %%%%%%%% 
    %% Passing the 'num_reuse'-best rollouts to the next iteration
    [~,index] = sort(sum(bR,2),'descend');
    index = index(1:Task.num_reuse);
    bR = bR(index,:);
    bsim_out = bsim_out(index);
    % Importance sampling trick for R
    for cnt = 1:length(bsim_out)
        sim_out = bsim_out(cnt);
        T = sim_out.t;
        X = sim_out.x;
        U = sim_out.u;
        Psi = sim_out.Controller.BaseFnc(T,X);  Psi = Psi(1:end-1,:);
        Psi = permute( repmat(Psi,[1 1 3]) ,[1 3 2] );
        
        
        o_theta = sim_out.Controller.theta(1:end-1,:);
        o_theta = repmat(o_theta,[1 1 length(T)]);
        n_theta = Controller.theta(1:end-1,:);
        n_theta = repmat(n_theta,[1 1 length(T)]);
        
        o_sigma = sim_out.Controller.theta(end,:);
        o_sigma = repmat(o_sigma,[1 1 length(T)]);
        n_sigma = Controller.theta(end,:);
        n_sigma = repmat(n_sigma,[1 1 length(T)]);
        
        thetaTpsi = sum(o_theta.*Psi,1);
        o_eT      = permute(U,[3 1 2]) - thetaTpsi;
        thetaTpsi = sum(n_theta.*Psi,1);
        n_eT      = permute(U,[3 1 2]) - thetaTpsi;
        
        temp = exp(sum(-(o_eT.*o_eT)./(2*o_sigma.^2),2));
        o_pr = (temp ./ prod(o_sigma,2))/(2*pi)^(3/2);
        o_pr = squeeze(o_pr);
        temp = exp(sum(-(n_eT.*n_eT)./(2*n_sigma.^2),2));
        n_pr = (temp ./ prod(n_sigma,2))/(2*pi)^(3/2);
        n_pr = squeeze(n_pr);
        
        IS = (n_pr ./ o_pr)';
        IS(end) = 1;
        bR(cnt,:) = IS .* bR(cnt,:);
    end
    % Substituting old Controller with new Controller
    for cnt = 1:length(bsim_out)
        bsim_out(cnt).Controller.theta = Controller.theta;
    end
    %%%%%%%%

end

t_cpu = cputime - t_cpu;
fprintf('CPU time: %f \n',t_cpu);

%% Final test simulation
sim_out = Ballbot_Simulator(eNAC_Model_Param,Task,Controller);
final_pose = [0.125*sim_out.x(9,end) -0.125*sim_out.x(7,end)]';
disp(['Center of the ball Final Pose: [' num2str(final_pose(1)) '; ' ...
    num2str(final_pose(2)) ']'])

%% Plots 
% Plot ballbot states and control inputs
myplots(sim_out)
% Plot learning curve
figure(4)
plot(0:length(AllCost)-1,AllCost)
xlabel('Iteration');  ylabel('Cost');  grid on

