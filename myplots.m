function myplots(sim_out)

 
figure(1);  clf
subplot(3,2,1);  plot(sim_out.t,sim_out.x(1,:))
xlabel('time [sec]');  ylabel('\theta_{x} [rad]');  grid on
subplot(3,2,2);  plot(sim_out.t,sim_out.x(2,:))
xlabel('time [sec]');  ylabel('d\theta_{x}/dt [rad/s]');  grid on
subplot(3,2,3);  plot(sim_out.t,sim_out.x(3,:))
xlabel('time [sec]');  ylabel('\theta_{y} [rad]');  grid on
subplot(3,2,4);  plot(sim_out.t,sim_out.x(4,:))
xlabel('time [sec]');  ylabel('d\theta_{y}/dt [rad/s]');  grid on
subplot(3,2,5);  plot(sim_out.t,sim_out.x(5,:))
xlabel('time [sec]');  ylabel('\theta_{z} [rad]');  grid on
subplot(3,2,6);  plot(sim_out.t,sim_out.x(6,:))
xlabel('time [sec]');  ylabel('d\theta_{z}/dt [rad/s]');  grid on

figure(2);  clf
subplot(2,2,1);  plot(sim_out.t,sim_out.x(7,:))
xlabel('time [sec]');  ylabel('\phi_{x} [rad]');  grid on
subplot(2,2,2);  plot(sim_out.t,sim_out.x(8,:))
xlabel('time [sec]');  ylabel('d\phi_{x}/dt [rad/s]');  grid on
subplot(2,2,3);  plot(sim_out.t,sim_out.x(9,:))
xlabel('time [sec]');  ylabel('\phi_{y} [rad]');  grid on
subplot(2,2,4);  plot(sim_out.t,sim_out.x(10,:))
xlabel('time [sec]');  ylabel('d\phi_{y}/dt [rad/s]');  grid on

figure(3);  clf
subplot(2,2,1);  plot(sim_out.t,sim_out.u(1,:))
xlabel('time [sec]');  ylabel('\tau_{1} [N.m]');  grid on
subplot(2,2,2);  plot(sim_out.t,sim_out.u(2,:))
xlabel('time [sec]');  ylabel('\tau_{2} [N.m]');  grid on
subplot(2,2,3);  plot(sim_out.t,sim_out.u(3,:))
xlabel('time [sec]');  ylabel('\tau_{3} [N.m]');  grid on

