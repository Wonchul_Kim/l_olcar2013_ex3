function function_call(my_path)
%
% This function executes the Matlab function 'Main_ILQG.m'
%
% my_path: path to the folder which contains 'Main_ILQG.m' 
%          e.g. function_call('D:\OLCAR Homework\HW#1\Codes')
%

load Model_Param

% Task structure
Task = struct;
Task.dt         = 0.02;
Task.start_x    = rand(10,1)*0.1-0.05;%zeros(10,1);
Task.start_time = 0;
Task.goal_pos   = [2 -0.8]';
Task.goal_vel   = [0 0]';
Task.goal_time  = 10;
Task.max_iteration = 20;
Task.random     = randn(3,Task.goal_time/Task.dt + 1,Task.max_iteration);

x_sym = sym(zeros(10,1));
u_sym = sym(zeros(3,1));
for cnt = 1:10
    x_sym(cnt)  = sym(sprintf('x%d',cnt),'real');
end
for cnt = 1:3
    u_sym(cnt)  = sym(sprintf('u%d',cnt),'real');
end
h = 100 *( (-0.125*x_sym(7)-Task.goal_pos(2))^2 + (0.125*x_sym(9)-Task.goal_pos(1))^2 ) + ...
    40*( (-0.125*x_sym(8)-Task.goal_vel(2))^2 + (0.125*x_sym(10)-Task.goal_vel(1))^2 ) + ...
    x_sym'*diag([40 4 40 4 40 4 0 0 0 0])*x_sym;
l = 10*x_sym(5)^2 + 1*x_sym(6)^2 + 0.1*(u_sym'*u_sym);

Cost   = struct;
Cost.x = x_sym;
Cost.u = u_sym;
Cost.h = h;
Cost.l = l;

Task.cost = Cost;

% Controller structure 
Controller = struct;

%% Execute 'Main_ILQG.m'
addpath(my_path)

[Controller,cost] = Main_ILQG (Model_Param, Task, Controller);

rmpath(my_path)

%% Test Algorithm Results
sim_out = Ballbot_Simulator(Model_Param,Task,Controller);

final_pose = [0.125*sim_out.x(9,end) -0.125*sim_out.x(7,end)]';
disp(['Center of the ball Final Pose: [' num2str(final_pose(1)) '; ' ...
    num2str(final_pose(2)) ']'])

figure(4)
plot(0:length(cost)-1,cost)
xlabel('Iteration');  ylabel('Cost');  grid on

end
